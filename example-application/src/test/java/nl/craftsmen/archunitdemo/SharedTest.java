package nl.craftsmen.archunitdemo;

import archunitdemo.SharedArchitectureTest;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchRules;
import com.tngtech.archunit.junit.ArchTest;

@AnalyzeClasses(packages = "nl.craftsmen.archunitdemo")
public class SharedTest {

    @ArchTest
    public static final ArchRules shared = ArchRules.in(SharedArchitectureTest.class);
}
