package nl.craftsmen.archunitdemo;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;
import static nl.craftsmen.archunitdemo.CustomRules.notHaveFieldsAnnotatedWithDeprecated;

@AnalyzeClasses(packages = "nl.craftsmen.archunitdemo")
public class DeprecatedFieldTest {

    @ArchTest
    public static final ArchRule deprecationRule = classes()
            .should(notHaveFieldsAnnotatedWithDeprecated);

}
