package nl.craftsmen.archunitdemo;

import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.library.dependencies.SlicesRuleDefinition.slices;

@AnalyzeClasses(packages = "nl.craftsmen.archunitdemo")
public class SliceTest {

    @ArchTest
    public static final ArchRule sliceRule = slices()
            .matching("..api.(*service).model")
            .should()
            .notDependOnEachOther();


}
